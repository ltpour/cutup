/*
  CUTUP: A command line cutup-machine

  Copyright 2018 Lasse Pouru

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <numeric>
#include <random>
#include <sstream>
#include <vector>

#define PROGRAM_NAME "Cutup"
#define PROGRAM_VERSION "0.1"

/* Globals */
std::string file;
unsigned pieces;

/* Option variables */
static const struct option longopts[] = {
    {"pieces",  required_argument, NULL, 'p'},
    {"help",    no_argument,       NULL, 'h'},
    {"version", no_argument,       NULL, 'v'},
    {NULL,      0,                 NULL, 0 }};

/* Prints help */
static void help (char* prgname) {
    std::cout << "Usage: " << prgname << " [OPTION]...\n";
    std::cout << "Make a cutup of a text file.\n\n";
    std::cout << "  -p, --pieces\tnumber of pieces to cut into\n";
    std::cout << "  -v, --help\tshow version information and exit\n";
    std::cout << "  -h, --help\tshow this help and exit\n";
}

/* Prints program version */
static void version (char* prgname) {
    std::cout << PROGRAM_NAME << " " PROGRAM_VERSION << std::endl;
    std::cout << "Copyright 2018 Lasse Pouru\n";
    std::cout << "License GPLv3: GNU GPL version 3 <http://gnu.org/licenses/gpl.html>\n";
}

/* Sets customizable variables */
static void parse_opts (int argc, char* argv[]) {

    /* Set option flags */
    int c = 0, h = 0, p = 0, v = 0;
    while((c = getopt_long(argc, argv, "hp:v", longopts, NULL)) != -1) {
	switch (c) {
	case 0:
	    break;
	case 'h': // help
	    h = 1;
	    break;
	case 'p':
	{ // pieces 
	    std::istringstream ss(optarg);
	    if (!(ss >> pieces) || pieces <= 0) {
		std::cerr << "Invalid number of pieces: " << optarg << '\n';
		exit(EXIT_FAILURE);
	    }
	    p = 1;
	}
	break;	
	case 'v': // version
	    h = 1;
	    break;
	default:
	    std::cout << "Try '" << argv[0] << " --help' for more information.\n";
	    exit(EXIT_FAILURE);
	}
    }

    /* Assign variables or display help or version information */
    if (h) {
	help(argv[0]);
	exit(EXIT_SUCCESS);
    }
    if (v) {
	version(argv[0]);
	exit(EXIT_SUCCESS);
    }
    if (!p)
	pieces = 1;

    /* Handle non-option arguments */
    file = argv[optind] ? argv[optind] : "";
    return;
}

int main(int argc, char *argv[]) {
    
    /* Parse options */
    parse_opts(argc, argv);

    /* Read file or input */
    std::string s;
    std::stringstream buffer;
    if (file != "") {
	std::ifstream t(file);
	buffer << t.rdbuf();	
    }
    else {
	while (std::cin >> s) {
	    buffer << s << " ";
	}
    }
    s = buffer.str();
    
    /* Output cutup */
    size_t size = s.size() - 1; // without terminating char
    if (pieces > size)
	pieces = size; // or: print error for too many pieces and exit?
    size_t piece = size / pieces;
    unsigned i = 0;
    std::vector<int> v(pieces - 1);
    std::iota(v.begin(), v.end(), 0);
    auto rng = std::default_random_engine {};
    std::shuffle(std::begin(v), std::end(v), rng);

    while (i < v.size()) {
	std::cout << s.substr(v.at(i) * piece, piece);
	++i;
    }
    std::cout << s.substr(i * piece, size - (piece * (pieces - 1)));
    
    /* Exit */
    exit(EXIT_SUCCESS);
}
