PROG	   = cutup
CC	   = g++
PREFIX	  ?= /usr/local

CFLAGS	= -Wall -std=c++14
LIBS	=

${PROG}: ${PROG}.c
	@${CC} ${CFLAGS} ${LIBS} -o ${PROG} ${PROG}.c
	@strip ${PROG}

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -Dm755 ${PROG} ${DESTDIR}${PREFIX}/bin/${PROG}

uninstall:
	rm -f ${PREFIX}/bin/${PROG}

clean:
	rm -f ${PROG}
