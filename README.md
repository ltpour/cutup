# CUTUP #

### Summary ###

A command line cut-up machine.

### Usage ###

Specify the number of pieces to cut into with the -p (--pieces) option. Input may be read from either a file, e.g. `cutup -p 64 textfile`, or a pipe, e.g. `ls | cutup -p 16`.

### Installation ###

`make && make install`
